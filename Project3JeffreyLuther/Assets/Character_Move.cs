﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Move : MonoBehaviour {
    public float walkSpeed = 6f;
    public float jumpSpeed = 4f;
    public float runSpeed = 12f;
    public float gravity = 9.8f;
    public bool useGravity = true;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController charController;
	// Use this for initialization
	void Start () {
        //get the CharacterController componet from this object
        charController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        //is the character controller grounded
        if (charController.isGrounded == true)
        {
            //set up moveDirection with input from the Horizontal(x) and Vertical axis(z)
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

            //Chang the axis to local coordinates
            moveDirection = transform.TransformDirection(moveDirection);

            //apply speed
            moveDirection *= GetSpeed();
            
            //pressing jump key?
            if(Input.GetButton("Jump"))
            {
                //add jumpSpeed to moveDirection.y
                moveDirection.y = jumpSpeed;
            }
            
        }

        //is the char using gravity
        if (useGravity == true)
        {
            //Subtrract from moveDirection.y accounting for frame-rate independence
            moveDirection.y -= gravity * Time.deltaTime;
        }
        //call move method on character controller
        charController.Move(moveDirection * Time.deltaTime);    
    }

    private float GetSpeed()
    {
        //is player pressing shift
        if(Input.GetKey(KeyCode.LeftShift))
        {
            //return runSpeed
            return runSpeed;
        }
        else
        {    
            //return walkSpeed
            return walkSpeed;
        } 
    }
}
