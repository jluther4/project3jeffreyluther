﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Very simple smooth mouselook modifier for the MainCamera in Unity
// This code was written for the final project last semester it is a slightly changed version and was originaly obtained from Francis R. Griffiths-Keam at www.runningdimensions.com 

public class CameraScript : MonoBehaviour
{
    public int waypoint;
    public int waypointNumber;

    public Transform defaultCameraPosition;

    Vector2 _mouseAbsolute;
    Vector2 _smoothMouse;

    public Vector2 clampInDegrees = new Vector2(180, 180);
    public bool lockCursor;
    public Vector2 sensitivity = new Vector2(2, 2);
    public Vector2 smoothing = new Vector2(3, 3);
    public Vector2 targetDirection;
    public Vector2 targetCharacterDirection;

    // Assign this if there's a parent object controlling motion, such as a Character Controller.
    // Yaw rotation will affect this object instead of the camera if set.
    public GameObject characterObject;

    void Start()
    {
        LockCursor();
        if (waypoint == waypointNumber && waypointNumber != 0)
        {
            // Set target direction to the camera's initial orientation.
            targetDirection = transform.localRotation.eulerAngles;

            // Set target direction for the character body to its inital state.
            if (characterObject) targetCharacterDirection = characterObject.transform.localRotation.eulerAngles;
        }

        else if (waypointNumber == 0)
        {
            Vector3 relativePos = defaultCameraPosition.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 2.0f);
        }

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LockCursor();
        }
        if (waypoint == waypointNumber && waypointNumber != 0)
        {
            // Allow the script to clamp based on a desired target value.
            var targetOrientation = Quaternion.Euler(targetDirection);
            var targetCharacterOrientation = Quaternion.Euler(targetCharacterDirection);

            // Get raw mouse input for a cleaner reading on more sensitive mice.
            var mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            // Scale input against the sensitivity setting and multiply that against the smoothing value.
            mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity.x * smoothing.x, sensitivity.y * smoothing.y));

            // Interpolate mouse movement over time to apply smoothing delta.
            _smoothMouse.x = Mathf.Lerp(_smoothMouse.x, mouseDelta.x, 1f / smoothing.x);
            _smoothMouse.y = Mathf.Lerp(_smoothMouse.y, mouseDelta.y, 1f / smoothing.y);

            // Find the absolute mouse movement value from point zero.
            _mouseAbsolute += _smoothMouse;

            // Clamp and apply the local x value first, so as not to be affected by world transforms.
            if (clampInDegrees.x < 360)
                _mouseAbsolute.x = Mathf.Clamp(_mouseAbsolute.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);

            var xRotation = Quaternion.AngleAxis(-_mouseAbsolute.y, targetOrientation * Vector3.right);
            transform.localRotation = xRotation;

            // Then clamp and apply the global y value.
            if (clampInDegrees.y < 360)
                _mouseAbsolute.y = Mathf.Clamp(_mouseAbsolute.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);

            transform.localRotation *= targetOrientation;

            // If there's a character object that acts as a parent to the camera
            if (characterObject)
            {
                var yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, characterObject.transform.up);
                characterObject.transform.localRotation = yRotation;
                characterObject.transform.localRotation *= targetCharacterOrientation;
            }
            else
            {
                var yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, transform.InverseTransformDirection(Vector3.up));
                transform.localRotation *= yRotation;
            }
        }

        else if (waypointNumber == 0)
        {
            Vector3 relativePos = defaultCameraPosition.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 2.0f);
        }

    }

    private void LockCursor()
    {
        //If cursor currently locked
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            //set it to not be locked
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            //set it to be locked
            Cursor.lockState = CursorLockMode.Locked;
        }
        //toggle cursor visablilty
        Cursor.visible = !Cursor.visible;
    }
}

