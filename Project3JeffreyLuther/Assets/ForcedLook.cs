﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcedLook : MonoBehaviour {

    public int waypoint;
    public int waypointNumber;

    public GameObject playerObject;
    public Transform target;

    public Transform defaultCameraPosition;
        
    void start()
    {
            
    }

    void Update()
    {
        if (waypoint == waypointNumber && waypointNumber !=0)
        {
            Vector3 relativePos = target.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 2.0f);
        }

        else if(waypointNumber == 0)
        {
            Vector3 relativePos = defaultCameraPosition.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 2.0f);
        }
    }
}
